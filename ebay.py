#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

from ws_backend import ws_backend
import re
import sys
import time
import mechanize
from bs4 import BeautifulSoup

class ebay(ws_backend):
    "ebay search backend class"

    def __init__(self, url, more_keywords, exclude_keywords, sleep_duration, title_only, new_entries, output):
        ws_backend.__init__(self, url, more_keywords, exclude_keywords, sleep_duration, new_entries)
        self.title_only = title_only
        self.categories = u"Animalerie"
        self.output = output
        ws_backend.backend_list.append("ebay")

    def search(self, keyword):
        if self.output:
            print "Searching '%s' on ebay" % keyword

        self.keyword = keyword
        response = self.__submit(keyword)
        soup = BeautifulSoup(response.read(), "lxml")

        if self.__parse_result_page(soup):
            while 1:
                try:
                    npage_link = soup.find("a", class_="gspr next")
                    np = ws_backend.browser.follow_link(url=npage_link['href'])
                    soup = BeautifulSoup(np.read(), "lxml")
                    self.__parse_result_page(soup)
                    if (self.sleep_duration):
                        time.sleep(self.sleep_duration)
                except:
                    #print sys.exc_info()
                    break


    def search_taxidermy(self, taxid_search, birds_tuple, taxid_key_tuple):
        q = ""
        for w in taxid_search:
            q = q + w + " "
        q = q[:len(q)-1]

        more_keywords_backup = self.more_keywords
        exclude_keywords_backup = self.exclude_keywords
        title_only_backup = self.title_only
        categories_backup = self.categories
        self.more_keywords = birds_tuple + taxid_key_tuple
        self.exclude_keywords = ()
        self.title_only = False
        self.categories = u"none"
        ws_backend.taxidermy_searched = True
        self.search(q)
        self.more_keywords = more_keywords_backup
        self.exclude_keywords = exclude_keywords_backup
        self.title_only = title_only_backup
        self.categories = categories_backup


#private methods

    def __submit(self, search):
        # there is only one form so select the first one
        ws_backend.browser.select_form(nr=0)
        s = search.encode("utf8")
        ws_backend.browser['_nkw'] = s
        # Select search option is set to:
        # "N'importe quels mots, peu importe l'ordre"
        # which makes sense if we look for several words
        # (like in search_taxidermy)
        ws_backend.browser['_in_kw'] = ['2']
        if "Animalerie" in self.categories:
            # We set category to Animalerie
            ws_backend.browser['_sacat'] = ['1281']
        if self.title_only == False:
            # Search in Title and Description
            # Default is a search in title only
            ws_backend.browser['LH_TitleDesc'] = ['1']
        # Located in France only (next two selects)
        ws_backend.browser['_fsradio2'] = ['&LH_LocatedIn=1']
        ws_backend.browser['_salic'] = ['71']

        response = ws_backend.browser.submit()

        return response

    # we look for keywords in category Animalerie only, but ebay extends the search to
    # all categories when nothing is found in Animalerie. We detect this thanks to this check.
    def __check_if_extended_research(self, soup):
        extended = soup.find("p", class_="sm-md")
        if extended:
            msg = extended.next_element.string
            match = re.match("Aucun r�sultat n'ayant �t� trouv� dans la cat�gorie", msg)
            if match:
                if self.output:
                    print "Aucun resultat"
                return True

        return False

    def __parse_result_page(self, soup):
        if self.__check_if_extended_research(soup):
            return False
        tl_result = soup.find_all("h3", class_="lvtitle")
        for result in tl_result:
            try:
                link = result.find_next("a")
                span = link.find("span")
                if span:
                    annonce = span.next_sibling.string
                else:
                    annonce = link.string
                rank = self.__rank_algorithm(annonce)
                if rank >= 1:
                    result = "ebay: " + annonce.strip() + " / " + link['href']
                    self.add_result(result, rank)
                    if self.output:
                        print "--------------------------"
                        print annonce.strip()
                        print link['href']
            except:
                #print sys.exc_info()
                continue

            return True


    def __rank_algorithm(self, title):
        rank = 0
        title = title.lower()
        for more_keyword in self.more_keywords:
            more_keyword = more_keyword.lower()
            if more_keyword in title:
                rank = rank + 1
        for exclude_keyword in self.exclude_keywords:
            exclude_keyword = exclude_keyword.lower()
            if exclude_keyword in title:
                rank = rank - 1

        #if self.title_only == False:
        #    if len(self.keyword.split()) > 1:
        #        for word in title.split():
        #            if word in self.keyword.lower():
        #                rank = rank + 1
        #                break
        #    else:
        #        if self.keyword.lower() in title:
        #            rank = rank + 1

        return rank
