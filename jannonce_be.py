#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

from ws_backend import ws_backend
import sys
import time
import mechanize
import urllib
from bs4 import BeautifulSoup

class jannonce_be(ws_backend):
    "jannonce.be search backend class"

    def __init__(self, url, more_keywords, exclude_keywords, sleep_duration, new_entries, output):
        ws_backend.__init__(self, url, more_keywords, exclude_keywords, sleep_duration, new_entries)
        self.categories = u"Animaux"
        self.output = output
        ws_backend.backend_list.append("jannonce_be")


    def search(self, keyword):
        if self.output:
            print "Searching '%s' on jannonce.be" % keyword

        self.keyword = keyword
        response = self.__submit(keyword)
        current_url = response.geturl()
        soup = BeautifulSoup(response.read(), "lxml")

        self.__parse_result_page(soup)
        while 1:
            try:
                npage_link = soup.find("a", title="Next page")
                # when the search return "O annonce" there is no
                # '<a title="next page">' tag
                if npage_link is None:
                    break
                # when the search returns more than 1 page of results
                # we test if last page by comparing current url
                # to next page url, which are in that case the same.
                if "http://www.jannonce.be" + npage_link['href'] == current_url:
                    break
                np = ws_backend.browser.follow_link(url=npage_link['href'])
                current_url = np.geturl()
                soup = BeautifulSoup(np.read(), "lxml")
                self.__parse_result_page(soup)
                if self.sleep_duration:
                    time.sleep(self.sleep_duration)
            except:
                break

#private methods

    def __submit(self, search):
        ws_backend.browser.select_form(nr=0)
        s = search.encode("utf-8")
        ws_backend.browser.form['q'] = s
        # Section = 74 means "Animaux"
        ws_backend.browser.form['Section'] = ["74"]
        ws_backend.browser.method = 'POST'
        response = ws_backend.browser.submit()

        return response


    def __parse_result_page(self, soup):
        result_list = soup.find("ul", class_="ad-list")
        for result in result_list.contents:
            link = result.find("a")
            # Following test might be surprising, the reason is that result might be a string
            # and in that case find method on a string returns -1 if substring is not
            # found. If result is a soup object, find method returns None if tag <a>
            # is not found
            if link is None or link == -1:
                continue
            title = link.contents[0].string
            title = title.strip()
            desc = result.find("p")
            title = title + ": " + desc.string.strip()
            location = result.find("span", class_="location")
            if location:
                location = location.string
            else:
                location = ""

            # do we fetch new entries only
            if self.new_entries:
                pass

            rank = self.__rank_algorithm(title, location)

            if rank >= 1:
                result = "jannonce.be: " + title + ": http://www.jannonce.be" + link['href']
                self.add_result(result, rank)
                if self.output:
                    print "---------------------------"
                    print rank
                    print title
                    print link['href']


    def __rank_algorithm(self, title, location):
        rank = 0
        title = title.lower()

        if location == "France":
            rank = rank + 1

        for more_keyword in self.more_keywords:
            more_keyword = more_keyword.lower()
            if more_keyword in title:
                rank = rank + 1
            for exclude_keyword in self.exclude_keywords:
                exclude_keyword = exclude_keyword.lower()
            if exclude_keyword in title:
                rank = rank - 1

        return rank
