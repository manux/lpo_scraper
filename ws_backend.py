#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import mechanize
import smtplib
import sys
from random import choice
from operator import itemgetter
from bs4 import BeautifulSoup
from email.mime.text import MIMEText

class ws_backend:
    "Base class for web backends"

    # mechanize browser
    browser = None

    # results are stored in a dictionary
    # { 'leboncoin:<annonce_title>/<category>:<annonce_url'(str):rank_value(int), .... }
    results = {}

    backend_list = []

    # category of birds we search:
    # can be: - passereaux: ensemble des passereaux dans la liste des oiseaux prot�g�s
    #         - others: autres que passereaux (tout ce qui n'est pas passereaux dans la liste des oiseaux prot�g�s)
    #         - all: tous (liste compl�te des oiseaux prot�g�s)
    birds_searched = ""

    # is True if we do a search on taxidermy specifically
    taxidermy_searched = False

    # - url: backend url
    # - more_keywords is an optional list of keywords: we search for a keyword,
    # but if in addition to the keyword, a keyword for this list is found the
    # ranking of the result is higher (see rank algorithm)
    # - sleep_duration: duration of sleep between 2 server requests
    def __init__(self, url, more_keywords, exclude_keywords, sleep_duration, new_entries):
        self.url = url
        self.more_keywords = more_keywords
        self.exclude_keywords = exclude_keywords
        self.sleep_duration = sleep_duration
        self.new_entries = new_entries
        if ws_backend.browser is None:
            self.__create_browser()

    def search(self, keyword):
        pass


    def search_taxidermy(self, taxid_search, birds_tuple, taxid_key_tuple):
        pass


    def open(self):
        if ws_backend.browser:
            ws_backend.browser.open(self.url)


    def add_result(self, value, rank):
        ws_backend.results[value] = rank


    def print_results(self):
        sorted_results = sorted(ws_backend.results.items(), key=itemgetter(1), reverse=True)
        for result in sorted_results:
            print "(%s) %s" % (result[1],result[0])


    def get_results(self, unicod):
        sorted_results = sorted(ws_backend.results.items(), key=itemgetter(1), reverse=True)
        string_result=""
        for result in sorted_results:
            string_result = string_result + "(" + str(result[1]) + ") " + result[0] + "\n"

        if not string_result:
            if self.new_entries:
                return u"Pas de nouveaux r�sultats"
            else:
                return u"Pas de r�sultats"

        if unicod == True:
            return string_result
        else:
            return string_result.encode("utf-8")


    def send_email_with_results(self, toaddr):
        try:
            server = smtplib.SMTP("localhost")
            #server.set_debuglevel(1)

            msg = u"Ce message a �t� g�n�r� automatiquement par lpo_scraper."
            backends = ""
            if len(self.backend_list) == 1:
                backends = self.backend_list[0]
            else:
                for backend_str in self.backend_list:
                    backends = backends + ", " + backend_str
            msg = msg + u" Les r�sultats sont issus d'une recherche sur " + backends + "."

            if ws_backend.birds_searched == "passereaux":
                msg = msg + u" La recherche porte sur l'ensemble des passereaux dans la liste des oiseaux prot�g�s."
            elif ws_backend.birds_searched == "others":
                msg = msg + u" La recherche porte sur l'ensemble des oiseaux de la liste des oiseaux prot�g�s, hors passereaux."
            elif ws_backend.birds_searched == "all":
                msg = msg + u" La recherche porte sur la liste compl�te des oiseaux prot�g�s."
            elif ws_backend.birds_searched == "test":
                msg = msg + u" La recherche porte sur une liste d'oiseaux utilis�e pour les tests."

            if ws_backend.taxidermy_searched:
                tax_msg = u" recherche sp�cifique de type taxidermie a �t� effectu�e pour l'ensemble des oiseaux et des mammif�res prot�g�s (y compris les oiseaux chassables mais dont le commerce est interdit)."
                if ws_backend.birds_searched == "none":
                    msg = msg + u" Une" + tax_msg
                else:
                    msg = msg +u" De plus, une" + tax_msg

            if self.new_entries:
                msg = msg + "\n----------------\n" + "Recherche seulement sur nouvelles annonces (datant d'Hier ou d'Aujourd'hui)" 

            msg = msg + "\n----------------\n\n" + self.get_results(True)

            email = MIMEText(msg.encode('utf-8'), 'plain', 'utf-8')
            email['Subject'] = "[LPO_SCRAPER]"
            email['From'] = "lpo_scraper@lpo.fr"
            email['To'] = toaddr

            server.sendmail("lpo_scraper@lpo.fr", toaddr, email.as_string())
            server.quit()
        except:
            print sys.exc_info()
            print "Failed to send email with results"


    def set_searched_birds(self, sbirds):
        ws_backend.birds_searched = sbirds

    def set_taxidermy_searched(self, taxidermy):
        ws_backend.taxidermy_searched = taxidermy

    # Discover web page forms:
    def discover_page_forms(self):
        for form in ws_backend.browser.forms():
            print "----------------------\n"
            print form


    def __create_browser(self):
        # browser stuff: we should have an anonymize browser here, see "Violent Python" chapter 6.
        user_agents = ['Mozilla/5.0 (X11; U; Linux; i686; en-US; rv:1.6) Gecko Debian/1.6-7','Konqueror/3.0-rc4; (Konqueror/3.0-rc4; i686 Linux;;datecode)','Opera/9.52 (X11; Linux i686; U; en)']
        random_user_agent = choice(user_agents)

        ws_backend.browser = mechanize.Browser(factory=mechanize.RobustFactory())
        #ws_backend.browser.add_handler(SanitizeHandler())
        ws_backend.browser.addheaders = [('User-agent', random_user_agent)]
        ws_backend.browser.set_handle_robots(False)
