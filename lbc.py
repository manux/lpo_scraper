#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

from ws_backend import ws_backend
import re
import sys
import time
import mechanize
import urllib
from bs4 import BeautifulSoup

class lbc(ws_backend):
    "LeBonCoin search backend class"


    # title_only is specific to LeBonCoin. It is to say whether we want to tick
    # "recherche dans le titre uniquement" (search in title only) which we
    # advise to do (i.e. title_only should be True)
    def __init__(self, url, more_keywords, exclude_keywords, sleep_duration, title_only, new_entries, output):
        ws_backend.__init__(self, url, more_keywords, exclude_keywords, sleep_duration, new_entries)
        self.title_only = title_only
        # Animaux must be first in the list of categories
        self.categories = u"Animaux", u"Collection", u"D�coration"
        self.__regex_categories = []
        self.__build_categories_regex_list()
        self.output = output
        self.fetch_category = True
        ws_backend.backend_list.append("LeBonCoin")

    def search(self, keyword):
        if self.output:
            print "Searching '%s' on LeBonCoin" % keyword

        self.keyword = keyword
        response = self.__submit(keyword)
        soup = BeautifulSoup(response.read(), "lxml")

        self.__parse_result_page(soup)
        while 1:
            try:
                npage_link = soup.find("a", id="next")
                np = ws_backend.browser.follow_link(url=npage_link['href'])
                soup = BeautifulSoup(np.read(), "lxml")
                self.__parse_result_page(soup)
                if (self.sleep_duration):
                    time.sleep(self.sleep_duration)
            except:
                break

    def search_taxidermy(self, taxid_search, birds_tuple, taxid_key_tuple):
        q = ""
        for w in taxid_search:
            q = q + w + " OR "
        q = q[:len(q)-4]

        more_keywords_backup = self.more_keywords
        exclude_keywords_backup = self.exclude_keywords
        title_only_backup = self.title_only
        fetch_category_backup = self.fetch_category
        self.more_keywords = birds_tuple + taxid_key_tuple
        self.exclude_keywords = ()
        self.title_only = False
        self.fetch_category = False
        ws_backend.taxidermy_searched = True
        self.search(q)
        self.more_keywords = more_keywords_backup
        self.exclude_keywords = exclude_keywords_backup
        self.title_only = title_only_backup
        self.fetch_category = fetch_category_backup


#private methods

    def __submit(self, search):
        #ws_backend.browser.select_form(name='f')
        #ws_backend.browser.select_form(nr=0)
        #s = search.encode("latin-1","ignore")
        #ws_backend.browser.form['q'] = s
        #if self.title_only == True:
        #    ws_backend.browser.form['it'] = ["1"]
        # w = 3 means "toute la france"
        #ws_backend.browser.form['w'] = ["3"]
        #ws_backend.browser.method = 'GET'
        #response = ws_backend.browser.submit(id="searchbutton")

        # Using mechanize forms methods doesn't work any more with new version
        # of Leboncoin. We couldn't find a better solution than doing it by
        # hand:
        s = search.encode("utf-8")
        if self.title_only == True:
            url = "https://www.leboncoin.fr/annonces/offres/?th=1&q=" + urllib.quote_plus(s) + "&it=1"
        else:
            url = "https://www.leboncoin.fr/annonces/offres/?th=1&q=" + urllib.quote_plus(s)
        response = ws_backend.browser.open(url)

        return response


    def __parse_result_page(self, soup):
        tl_result = soup.find_all("section", class_="item_infos")
        for result in tl_result:
            title = result.find("h2", class_="item_title")
            # title might not always be there, this is the case for 'les annonces � la une'
            # which are redundant
            if title is None:
                continue
            # we detect here if we have a <span class="item_sup booster orange">...
            # in that case title text is just before <span ...>
            span = title.find("span")
            if span:
                title = span.previous_element

            category = result.find("p", class_="item_supp")
            span = category.find("span")
            # we detect here if we have a <span class="ispro">
            # which might be inserted before the category string
            try:
                if span:
                    category_l = span.next_element.string.split()
                else:
                    category_l = category.string.split()
            except: # there is no category
                category_l = ['unknown category']

            # do we fetch new entries only (new entries are those marked as "Hier" or "Aujourd'hui")
            if self.new_entries:
                aside = result.find("aside", class_="item_absolute")
                date = aside.find("p", class_="item_supp")
                date = date.string.strip()
                day = date.split(",")[0].strip()
                if day not in [u"Hier", u"Aujourd'hui"]:
                    continue

            if self.fetch_category:
                rank = self.__rank_algorithm(title.string, category_l[0])
            else:
                rank = self.__rank_algorithm(title.string, None)

            if rank >= 1:
                link = result.find_previous("a")
                if len(category_l) > 1:
                    category_s = ""
                    for w in category_l:
                        category_s = category_s + " " + w
                else:
                    category_s = category_l[0]

                result = "leboncoin: " + title.string.strip() + " / " + category_s + ": http:" + link['href']
                self.add_result(result, rank)
                if self.output:
                    print "---------------------------"
                    print rank
                    print title.string.strip()
                    print link['href']
                    print category_s


    def __rank_algorithm(self, title, category):
        rank = 0
        an = False
        ad = False
        title = title.lower()
        if category:
            if self.__regex_categories[0].search(category):
                # +2 if "Animaux" is the category
                rank = rank + 2
                an = True

        for more_keyword in self.more_keywords:
            more_keyword = more_keyword.lower()
            if more_keyword in title:
                ad = True
                rank = rank + 1
        for exclude_keyword in self.exclude_keywords:
            exclude_keyword = exclude_keyword.lower()
            if exclude_keyword in title:
                rank = rank - 1

        if category:
            if ad == True and an == False or self.new_entries:
                for cat_regex in self.__regex_categories[1:]:
                    if cat_regex.search(category):
                        rank = rank + 1

        return rank


    def __build_categories_regex_list(self):
        for cat in self.categories:
            self.__regex_categories.append(re.compile(cat))
