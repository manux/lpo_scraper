#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

from lbc import lbc
from ebay import ebay
from jannonce_be import jannonce_be
import sys
import time
import mechanize
import argparse

birds_all = u"accenteur", u"agrobate", u"aigle royal", u"aigle fauve", u"aigle dor�", u"aigrette", u"albatros", u"alcyon", u"alouette", u"alque", u"ammomane", u"arlequin plongeur", u"garrot arlequin", u"autour des palombes", u"autour sombre", u"avocette", u"balbuzard", u"barge", u"bartramie", u"bec-crois�", u"bergeronnette", u"bernache", u"bihoreau", u"blongios", u"bondr�e", u"bouscarle", u"bouvreuil", u"bruant", u"bulbul", u"busard", u"buse f�roce", u"buse variable", u"buse pattue", u"buse d'am�rique", u"butor", u"b�casse", u"b�casseau", u"b�cassin", u"b�cassine", u"calliope", u"canard faucille", u"sarcelle faucille", u"canard front blanc", u"canard siffleur", u"canard jansen", u"canard noir", u"canard noir�tre", u"cardinal poitrine rose", u"gros bec poitrine rose", u"carouge", u"casse-noix mouchet�", u"chardonneret", u"chauve souris", u"chauve-souris", u"chevalier", u"chev�che", u"chev�chette", u"chocard", u"choucas", u"chouette oural", u"chouette tengmalm", u"nyctale bor�ale", u"chouette �pervi�re", u"sturnie �pervi�re", u"chouette hulotte", u"chouette lapone", u"cigogne blanche", u"cigogne noire", u"cincle", u"circa�te", u"cisticole", u"cochevis", u"corbeau familier", u"cormoran", u"corneille", u"coucou geai", u"coucou gris", u"coulicou", u"courlis", u"courvite", u"crabier", u"crave", u"cr�cerelle", u"cygne chanteur", u"cygne sauvage", u"cygne bewick", u"cygne siffleur", u"cygne tubercul�", u"cygne muet", u"damier cap", u"dendrocygne", u"dickcissel", u"durbec", u"echasse blanche", u"effraie clocher", u"chouette effraie", u"eider", u"elanion", u"engoulevent", u"epervier", u"erismature", u"etourneau roselin", u"martin roselin", u"etourneau unicolore", u"faucon", u"fauvette", u"flamant", u"fou bassan", u"fou cap", u"fou masqu�", u"foulque", u"francolin", u"fr�gate", u"fuligule", u"fulmar", u"ganga", u"garrot alb�ole", u"garrot islande", u"glar�ole", u"gobemouche", u"goglu", u"gorgebleue", u"go�land", u"grand corbeau", u"grand labbe", u"grand-duc", u"grand duc", u"gravelot", u"grimpereau", u"grive", u"grosbec", u"grue cendr�e", u"grue demoiselle", u"demoiselle numidie", "grue canada", u"gr�be", u"guiaraca", u"guifette", u"guillemot", u"gu�pier", u"gypa�te", u"harfang", u"harle", u"hibou marais", u"hibou brachyote", u"hibou cap", u"hibou moyen-duc", u"moyen duc", u"hirondelle", u"huppe fasci�e", u"hypola�s", u"h�ron", u"ibis", u"iranie", u"jaseur", u"junco", u"labbe", u"linotte", u"locustelle", u"loriot", u"lusciniole", u"macareux", u"macreuse", u"marouette", u"martin pecheur", u"martin chasseur", u"martinet", u"mergule", u"merle plastron", u"merle am�rique", u"merle migrateur", u"merle unicolore", u"milan noir", u"milan royal", u"monticole", u"moqueur", u"moucherolle", u"mouette", u"m�sange", u"m�sangeai", u"niverolle", u"noddi", u"oc�anite", u"oedicn�me", u"oie bec court", u"oie ross", u"oie neiges", u"oie naine", u"oriole", u"outarde", u"panure", u"paruline", u"passerin", u"petit-duc", u"petit duc", u"phalarope", u"pha�ton", u"phragmite", u"pic dos blanc", u"pic cendr�", u"pic �peiche", u"pic �peichette", u"pic flamboyant", u"pic macul�", u"pic mar", u"pic noir", u"pic syriaque", u"pic tridactyle", u"pic vert", u"pivert", u"pie bleue", u"pie-gri�che", u"pigeon lauriers", u"pigeon bolle", u"pigeon trocaz", u"pingouin torda", u"pingouin petit", u"pinson", u"pipit", u"plongeon", u"pluvian", u"pluvier", u"pouillot", u"puffin", u"pygargue", u"p�lican", u"p�trel", u"quiscale", u"robin", u"roitelet", u"rollier", u"roselin", u"rossignol bleu", u"rossignol philom�le", u"rossignol progn�", u"rouge gorge", u"rouge-gorge", u"rougequeue", u"rousserolle", u"r�le", u"r�miz", u"sarcelle ailes", u"sarcelle soucrourou", u"sarcelle caroline", u"sarcelle �l�gante", u"sarcelle marbr�e", u"serin", u"sirli", u"sittelle", u"sizerin", u"spatule blanche", u"starique", u"sterne", u"syrrhapte", u"tadorne", u"tal�ve", u"tangara", u"tarier", u"tchagra", u"tichodrome", u"tohi", u"torcol", u"tournepierre", u"traquet", u"troglodyte", u"turnix", u"vacher", u"vanneau", u"vautour", u"venturon", u"verdier", u"vir�o"

#those birds have been removed from birds lists because give too many results and nothing relevant.
#removed_birds=u"tourterelle", u"moineau"

birds_passereaux = u"accenteur", u"agrobate", u"alouette", u"ammomane", u"bec-crois�", u"bec crois�", u"bergeronnette", u"bouscarle", u"bouvreuil", u"bruant", u"calliope", u"cardinal poitrine rose", u"carouge", u"casse-noix mouchet�", u"casse noix mouchet�", u"chardonneret", u"chocard", u"choucas", u"cincle", u"cisticole", u"cochevis", u"corbeau familier", u"corneille", u"crave", u"dickcissel", u"durbec", u"etourneau unicolore", u"etourneau resolin", u"martin roselin", u"fauvette", u"gobemouche", u"goglu", u"gorgebleue", u"gorge bleue", u"grand corbeau", u"grimpereau", u"grive", u"grosbec", u"gros bec", u"guiaraca", u"hirondelle", u"hypola�s", u"iranie", u"jaseur", u"junco", u"linotte", u"locustelle", u"loriot", u"lusciniole", u"merle plastron", u"merle am�rique", u"merle migrateur", u"merle unicolore", u"m�sange", u"m�sangeai", u"monticole", u"moqueur", u"moucherolle", u"niverolle", u"oriole", u"ortolan", u"panure", u"paruline", u"passerin", u"phragmite", u"\"pie bleue\"", u"pie-gri�che", u"pie gri�che", u"pinson", u"pipit", u"pouillot", u"quiscale", u"r�miz", u"robin", u"roselin", u"roitelet", u"rossignol bleu", u"rossignol philom�le", u"rossignol progn�", u"rouge gorge", u"rouge-gorge", u"rougequeue", u"\"rouge queue\"", u"rousserolle", u"serin", u"sirli", u"sittelle", u"sizerin", u"tangara", u"tarier", u"tarin", u"tchagra", u"tichodrome", u"tohi", u"traquet", u"troglodyte", u"vacher", u"venturon", u"verdier", u"vir�o"

birds_others = u"aigle royal", u"aigle fauve", u"aigle dor�", u"aigrette", u"albatros", u"alcyon", u"alque", u"arlequin plongeur", u"garrot arlequin", u"autour des palombes", u"autour sombre", u"avocette", u"balbuzard", u"barge", u"bartramie", u"bernache", u"bihoreau", u"blongios", u"bondr�e", u"bulbul", u"busard", u"buse f�roce", u"buse variable", u"buse pattue", u"buse d'am�rique", u"butor", u"b�casse", u"b�casseau", u"b�cassin", u"b�cassine", u"canard faucille", u"sarcelle faucille", u"canard front blanc", u"canard siffleur", u"canard jansen", u"canard noir", u"canard noir�tre", u"gros bec poitrine rose", u"chauve souris", u"chauve-souris", u"chevalier", u"chev�che", u"chev�chette", u"chouette oural", u"chouette tengmalm", u"nyctale bor�ale", u"chouette �pervi�re", u"sturnie �pervi�re", u"chouette hulotte", u"chouette lapone", u"cigogne blanche", u"cigogne noire", u"circa�te", u"cormoran", u"coucou geai", u"coucou gris", u"coulicou", u"courlis", u"courvite", u"crabier", u"cr�cerelle", u"cygne chanteur", u"cygne sauvage", u"cygne bewick", u"cygne siffleur", u"cygne tubercul�", u"cygne muet", u"damier cap", u"dendrocygne", u"echasse blanche", u"effraie clocher", u"chouette effraie", u"eider", u"elanion", u"engoulevent", u"epervier", u"erismature", u"faucon", u"flamant", u"fou bassan", u"fou cap", u"fou masqu�", u"foulque", u"francolin", u"fr�gate", u"fuligule", u"fulmar", u"ganga", u"garrot alb�ole", u"garrot islande", u"glar�ole", u"go�land", u"grand labbe", u"grand-duc", u"grand duc", u"gravelot", u"grue cendr�e", u"grue demoiselle", u"demoiselle numidie", "grue canada", u"gr�be", u"guifette", u"guillemot", u"gu�pier", u"gypa�te", u"harfang", u"harle", u"hibou marais", u"hibou brachyote", u"hibou cap", u"hibou moyen-duc", u"moyen duc", u"huppe fasci�e", u"h�ron", u"ibis", u"labbe", u"macareux", u"macreuse", u"marouette", u"martin pecheur", u"martin chasseur", u"martinet", u"mergule", u"milan noir", u"milan royal", u"mouette", u"noddi", u"oc�anite", u"oedicn�me", u"oie bec court", u"oie ross", u"oie neiges", u"oie naine", u"outarde", u"petit-duc", u"petit duc", u"phalarope", u"pha�ton", u"pic dos blanc", u"pic cendr�", u"pic �peiche", u"pic �peichette", u"pic flamboyant", u"pic macul�", u"pic mar", u"pic noir", u"pic syriaque", u"pic tridactyle", u"pic vert", u"pivert", u"pigeon lauriers", u"pigeon bolle", u"pigeon trocaz", u"pingouin torda", u"pingouin petit", u"plongeon", u"pluvian", u"pluvier", u"puffin", u"pygargue", u"p�lican", u"p�trel", u"rollier", u"r�le", u"sarcelle ailes", u"sarcelle soucrourou", u"sarcelle caroline", u"sarcelle �l�gante", u"sarcelle marbr�e", u"spatule blanche", u"starique", u"sterne", u"syrrhapte", u"tadorne", u"tal�ve", u"torcol", u"tournepierre", u"turnix", u"vanneau", u"vautour"

#birds_test = u"ortolan", u"chardonneret", u"h�ron"
#birds_test = u"h�ron", u"chardonneret", u"alouette"
birds_test = u"h�ron",

more_keywords = u"oiseau", u"oiseaux", u"sauvages", u"sauvage", u"naturalis�", u"naturalis�e", u"naturaliser", u"taxidermie", u"ornithologie", u"empaill�", u"empaill�e", u"empaill�s", u"empaill�es", u"empailler", u"troph�e", u"troph�"

exclude_keywords = u"voli�re", u"voliere", u"graines", u"m�lange", u"graisse", u"nichoir", u"nichoirs", u"mangeoire", u"bo�te", u"nid", u"nids", u"nidification", u"fontaine", u"bain", u"feeder", u"canary", u"canaries", u"perruche", u"perruches", u"nourriture", u"semence", u"semences", u"statuette", u"guide", u"tableau", u"toile", u"toiles", u"artiste", u"tableaux", u"cadres", u"cadre", u"linge", u"timbre", u"timbres", u"grit", u"livre", u"livres", u"porcelaine", u"porcelaines", u"vase", u"vases", u"chien", u"chiens", u"saillie", u"chiot", u"chiots", u"jouet", u"jouets", u"plastic", u"plastique", u"elevage", u"�levage", u"statue", u"doudou", u"mug", u"mugs", u"autocollant", u"playmobil", u"robe", u"robes", u"statuette", u"figurine", u"feve", u"f�ve", u"riga", u"menu", u"natu'riga", u"bogena", u"beaphar", u"intensief", u"blattner", u"carte"


# Taxidermie search is the last search done when all the different birds have been searched
# The different strings are searched together (using an OR for instance)
taxidermy_search = u"naturalis�", u"empaill�", u"taxidermie"
# for each single result we get from the "taxidermie search" in addition to birds names (from related birds list) we look for taxidermie keywords below
taxidermy_birds = u"oiseau", u"oiseaux", u"aigle", u"buse", u"canard", u"cardinal", u"cygne", u"coucou", u"chouette", u"echasse", u"hibou", u"merle", u"pic", u"pingouin", u"rossignol", u"sarcelle", u"spatule"
taxidermy_mammals = u"fouine", u"martre", u"hermine", u"belette", u"putois", u"lievre", u"li�vre", u"marmotte"
taxidermy_keywords = taxidermy_birds + taxidermy_mammals


parser = argparse.ArgumentParser()
parser.add_argument('-l', '--birdlist', choices=['passereaux','others','all', 'test', 'none'], default='none')
parser.add_argument('-b', '--backend', action ='append', choices=['leboncoin','ebay', 'jannonce_be'], required=True)
parser.add_argument('-m', '--mail', action='append')
parser.add_argument('-t', '--taxidermy', action='store_true', default=False)
parser.add_argument('-n', '--new_entries', action='store_true', default=False)
parser.add_argument('-f', '--forms', action='store_true', default=False)
parser.add_argument('--output', action='store_true', default=False)

args = parser.parse_args()

backend_list=[]
for backend in args.backend:
    if backend == "leboncoin":
        b = lbc("http://www.leboncoin.fr/annonces/offres/", more_keywords, exclude_keywords, 1, True, args.new_entries, args.output)
    elif backend == "ebay":
        b = ebay("http://www.ebay.fr/sch/ebayadvsearch", more_keywords, exclude_keywords, 1, True, args.new_entries, args.output)
    elif backend == "jannonce_be":
        b = jannonce_be("http://www.jannonce.be/", more_keywords, exclude_keywords, 1, args.new_entries, args.output)
    backend_list.append(b)

# internal usage option
# in that case only one backend must be given (using -l), otherwise last backend given in
# -l option is used.
if args.forms:
    b.open()
    b.discover_page_forms()
    sys.exit(0)


if args.birdlist == "passereaux":
    bird_list = birds_passereaux
elif args.birdlist == "others":
    bird_list = birds_others
elif args.birdlist == "all":
    bird_list = birds_all
elif args.birdlist == "test":
    bird_list = birds_test
elif args.birdlist == "none":
    bird_list = ()
else:
    args.birdlist = "none"
    bird_list = ()

b.set_searched_birds(args.birdlist)

for b in backend_list:
    b.open()
    for bird in bird_list:
        try:
            b.search(bird)
            continue
        except:
            time.sleep(1)
            continue
        time.sleep(4)
    if args.taxidermy:
        b.search_taxidermy(taxidermy_search, birds_all, taxidermy_keywords)


if args.output:
    backend_list[0].print_results()

# TODO:
# check that each args.mail is a valid email@
if args.mail:
    for email in args.mail:
        backend_list[0].send_email_with_results(email)
